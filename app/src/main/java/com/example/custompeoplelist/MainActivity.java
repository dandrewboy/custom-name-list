package com.example.custompeoplelist;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    Button b_add, b_ABC, b_AGE;
    ListView lv_friendsList;


    PersonAdapter adapter;
    MyFriends myFriends;

    public void editPerson(int position) {
        Intent i = new Intent(getApplicationContext(), NewPersonForm.class);

        Person p = myFriends.getMyFriendsList().get(position);

        i.putExtra("name", p.getName());
        i.putExtra("age", p.getAge());
        i.putExtra("iconNumber", p.getIconNumber());
        i.putExtra("edit", position);

        startActivity(i);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_add = findViewById(R.id.b_add);
        b_ABC = findViewById(R.id.b_sortByName);
        b_AGE = findViewById(R.id.b_sortByAge);
        lv_friendsList = findViewById(R.id.lv_names);

        myFriends = ((MyApplication) this.getApplication()).getMyFriends();

        adapter = new PersonAdapter(MainActivity.this, myFriends);

        lv_friendsList.setAdapter(adapter);

        Bundle incomingMessage = getIntent().getExtras();
        if(incomingMessage != null) {
            String name = incomingMessage.getString("name");
            int age = Integer.parseInt(incomingMessage.getString("age"));
            int icon = Integer.parseInt(incomingMessage.getString("IconNumber"));
            int editedPerson = incomingMessage.getInt("edit");

            Person p = new Person(name, age, icon);


            if(editedPerson > -1) {
                myFriends.getMyFriendsList().remove(editedPerson);
            }
            myFriends.getMyFriendsList().add(p);

            adapter.notifyDataSetChanged();
        }



        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), NewPersonForm.class);
                startActivity(i);
            }
        });

        b_ABC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(myFriends.myFriendsList);
                adapter.notifyDataSetChanged();
            }
        });

        b_AGE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(myFriends.myFriendsList, new Comparator<Person>() {
                    @Override
                    public int compare(Person p1, Person p2) {
                        return p1.getAge() - p2.getAge();
                    }
                });

                adapter.notifyDataSetChanged();
            }
        });



        lv_friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editPerson(position);
            }
        });






    }
}
