package com.example.custompeoplelist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewPersonForm extends AppCompatActivity {

    Button b_ok, b_cancel;
    EditText et_name, et_age, et_iconNumber;
    int editedPerson = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person_form);

        b_ok = findViewById(R.id.b_ok);
        b_cancel = findViewById(R.id.b_cancel);
        et_name = findViewById(R.id.et_name);
        et_age = findViewById(R.id.et_age);
        et_iconNumber = findViewById(R.id.et_iconNumber);

        Bundle incomingIntent = getIntent().getExtras();

        if(incomingIntent != null) {
            String name = incomingIntent.getString("name");
            int age = incomingIntent.getInt("age");
            int icon = incomingIntent.getInt("iconNumber");
            editedPerson = incomingIntent.getInt("edit");

            et_name.setText(name);
            et_age.setText(Integer.toString(age));
            et_iconNumber.setText(Integer.toString(icon));
        }

        b_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newName = et_name.getText().toString();
                String newAge = et_age.getText().toString();
                String newIcon = et_iconNumber.getText().toString();



                Intent i = new Intent(v.getContext(), MainActivity.class);

                i.putExtra("edit", editedPerson);
                i.putExtra("name", newName);
                i.putExtra("age", newAge);
                i.putExtra("IconNumber", newIcon);

                startActivity(i);
            }
        });
    }
}
