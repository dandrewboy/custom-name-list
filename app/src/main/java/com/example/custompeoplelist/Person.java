package com.example.custompeoplelist;

public class Person implements Comparable<Person>{

    public String name;
    public int age;
    public int iconNumber;

    public Person(String name, int age, int iconNumber) {
        this.name = name;
        this.age = age;
        this.iconNumber = iconNumber;
    }

    @Override
    public int compareTo(Person other) {
        return this.name.compareTo(other.name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getIconNumber() {
        return iconNumber;
    }

    public void setIconNumber(int iconNumber) {
        this.iconNumber = iconNumber;
    }


}
